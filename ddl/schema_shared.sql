-- drop schema if exists shared cascade;
-- create schema shared;

create table data.artist(
    id serial primary key unique,
    name varchar unique
);
create index artist_name on data.artist(name);

create table data.album(
    id serial primary key unique,
    artist_id int references data.artist(id) not null,
    name varchar
);
create index album_name on data.album(name);

create table data.song(
    id serial primary key unique,
    album_id int references data.album(id) null,
    artist_id int references data.artist(id) not null,
    name varchar
);
create index song_name on data.song(name);

