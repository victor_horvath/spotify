create table data.version (
    id serial primary key unique,
	name varchar,
    description varchar,
    ts_from timestamp,
    ts_inserted timestamp default now()
);

create table data.sample_table (
    id serial primary key unique,
    version_id int REFERENCES data.version(id) not null
    
);

create table data.userdata (
    id serial primary key unique,
    version_id int REFERENCES data.version(id) not null,
    username varchar,
    email varchar,
    country varchar,
    created_from_facebook boolean,
    facebook_uid varchar,
    birthdate date,
    gender varchar, -- FIXME: Datenbanknormalisierung
    postal_code varchar,
    mobile_number varchar,
    mobile_operator varchar,
    mobile_brand varchar,
    creation_date date
);

create table data.streaming_history (
    id serial primary key unique,
    version_id int REFERENCES data.version(id) not null,
    end_time timestamp,
    ms_played int,
    song_id int references data.song(id)
);




