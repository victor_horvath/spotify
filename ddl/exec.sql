\cd ddl

begin;
drop schema if exists data cascade;
create schema data;

\i schema_shared.sql
\i schema_user_data.sql
-- \i views.sql
commit;

\cd ..