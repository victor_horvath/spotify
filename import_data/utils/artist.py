class ArtistCache(dict):
    pass


def get_artist(db, artist_name):
    # resolve class
    Artist = db.classes["artist"]
    # query table
    query = db.session.query(Artist)
    return query.filter(Artist.name == artist_name).one_or_none()


def get_or_create_artist(db, artist_name):
    if ArtistCache not in db.caches:
        db.caches[ArtistCache] = ArtistCache()

    sc = db.caches[ArtistCache]

    # Check if object is cached
    if artist_name not in sc:
        # check if the artist is known
        artist = get_artist(db, artist_name)
        if artist is None:
            # resolve class
            Artist = db.classes["artist"]
            artist = Artist(name=artist_name)
            db.session.add(artist)
            # flush here because because the ids are needed
            db.session.flush()
        # add artist to cache
        sc[artist_name] = artist

    return sc[artist_name]
