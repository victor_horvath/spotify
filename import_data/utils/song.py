from .artist import get_or_create_artist


class SongCache(dict):
    pass


def get_or_create_song(db, songname, artist_name):
    if SongCache not in db.caches:
        db.caches[SongCache] = SongCache()

    sc = db.caches[SongCache]
    key = (songname, artist_name)

    # Check if object is cached
    if key not in sc:
        # resolve class
        Song = db.classes["song"]
        # get the artist
        artist = get_or_create_artist(db, artist_name)
        # try to get an existing song
        query = db.session.query(Song)
        query = query.filter(
            Song.artist == artist, Song.name == songname)
        song = query.one_or_none()
        # create a new song if not existing
        if song is None:
            song = Song(
                name=songname,
                artist=artist
            )
            db.session.add(song)
        sc[key] = song

    return sc[key]
