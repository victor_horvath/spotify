import json
from datetime import datetime


_column_json_map = {
    "username": "username",
    "email": "email",
    "country": "country",
    "created_from_facebook": "createdFromFacebook",
    "facebook_uid": "facebookUid",
    "gender": "gender",
    "postal_code": "postalCode",
    "mobile_number": "mobileNumber",
    "mobile_operator": "mobileOperator",
    "mobile_brand": "mobileBrand"
}


def import_user_data(db, vers, f):
    # resolve required classes
    Userdata = db.classes["userdata"]
    # parse input
    data = json.load(f)
    columns = dict(
        (key, data[field])
        for key, field in _column_json_map.items()
    )
    columns["birthdate"] = datetime.strptime(
        data["birthdate"], "%Y-%m-%d").date()
    columns["creation_date"] = datetime.strptime(
        data["creationTime"], "%Y-%m-%d").date()

    db.session.add(Userdata(version=vers, **columns))
