import re


from .userdata import import_user_data
from .streaming_history import import_streaming_history

_simple_map = {
    "MyData/Userdata.json": import_user_data
}


def process_file(db, vers, f_name, f):
    print(f"Processing {f_name}")
    if f_name in _simple_map.keys():
        _simple_map[f_name](db, vers, f)
    elif re.match("MyData/StreamingHistory[0-9]+.json", f_name):
        import_streaming_history(db, vers, f)
