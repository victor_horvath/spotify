import json
from datetime import datetime

from import_data.utils.song import get_or_create_song


END_TIME_FORMAT = "%Y-%m-%d %H:%M"  # format is "2019-12-20 15:12"


def import_streaming_history(db, vers, f):
    # resolve classes
    StreamingHistory = db.classes["streaming_history"]
    # parse data
    data = json.load(f)
    for entry in data:
        song = get_or_create_song(db, entry["trackName"], entry["artistName"])
        sh = StreamingHistory(
            version=vers,
            end_time=datetime.strptime(entry["endTime"], END_TIME_FORMAT),
            song=song,
            ms_played=entry["msPlayed"]
        )
        db.session.add(sh)
