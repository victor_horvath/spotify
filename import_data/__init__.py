import json
from zipfile import ZipFile

from .files import process_file


def import_zip(db, path_to_file):
    # resolve orm classes
    Version = db.classes["version"]
    # start session
    db.session.begin()
    # create an import version
    vers = Version(name="Testversion")
    db.session.add(vers)

    with ZipFile(path_to_file, "r") as zipf:
        for info in zipf.infolist():
            process_file(db, vers, info.filename, zipf.open(info))

    db.session.commit()
