from sqlalchemy import create_engine

import argparse
from . import import_zip
from .db_session import DBCon

parser = argparse.ArgumentParser(
    description="""Import your spotify data to db"""
)

parser.add_argument(
    '--file', '-f', type=str,
    help="zip File with data", required=True,
)
parser.add_argument(
    "--database", "-d", type=str,
    help="Database to be used. Default ist spotify_dev", default="spotify_dev",
)

args = parser.parse_args()
db = DBCon(args.database)

import_zip(db, args.file)
