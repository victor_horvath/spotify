from sqlalchemy import create_engine
from sqlalchemy.orm import create_session
from sqlalchemy.schema import MetaData
from sqlalchemy.ext.automap import automap_base


class DBCon(object):
    def __init__(self, db):
        self._engine = create_engine(
            f"postgresql://postgres:postgres@localhost:5432/{db}"
        )
        self._session = None
        self._meta = None
        self._base = None
        self.caches = dict()

    @property
    def session(self):
        if self._session is None:
            self._session = create_session(bind=self._engine)
        return self._session

    @property
    def classes(self):
        if self._meta is None:
            self._reflect()
        return self._base.classes

    def _reflect(self):
        self._meta = MetaData(bind=self._engine, schema="data")
        self._meta.reflect(views=True)
        self._base = automap_base(
            bind=self._engine, metadata=self._meta)
        self._base.prepare()
