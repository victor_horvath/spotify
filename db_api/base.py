from sqlalchemy.ext.declarative import as_declarative, declarative_base, declared_attr
from sqlalchemy import Column, Integer

Base = declarative_base()


# @as_declarative()
# class Base(object):
#     @declared_attr
#     def id(cls):
#         Column(Integer, primary_key=True)


def create_db(engine):
    Base.metadata.create_all(engine)
