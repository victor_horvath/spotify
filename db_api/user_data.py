from sqlalchemy import Column, ForeignKey, Integer, String, TIMESTAMP, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declared_attr

from .base import Base


class Version(Base):
    __tablename__ = 'version'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    ts_from = Column(TIMESTAMP)
    ts_inserted = Column(TIMESTAMP)


class VersionedTable(Base):
    __abstract__ = True
    @declared_attr
    def version_id(self):
        return Column(Integer, ForeignKey(Version.id), nullable=False)

    @declared_attr
    def version(self):
        return relationship("Version", foreign_keys='id')


class Userdata(VersionedTable):
    __tablename__ = 'userdata'
    id = Column(Integer, primary_key=True)
    username = Column(String)
    email = Column(String)
    created_from_facebook = Column(Boolean)
    facebook_uid = Column(String)
    birthdate = Column(TIMESTAMP)
    gender = Column(String)
    postal_code = Column(String)
    mobile_number = Column(String)
    mobile_operator = Column(String)
    mobile_brand = Column(String)
    creation_date = Column(Boolean)


# class StreamingHistory(Base):
class StreamingHistory(Base):
    __tablename__ = 'streaming_history'
    id = Column(Integer, primary_key=True)
    end_time = Column(TIMESTAMP)
    ms_played = Column(Integer)
